#reactapp-build

# Imagen base sobre la cual construiremos nuestra app
FROM node:12-alpine as build

# 
WORKDIR /app

# Copiamos la app al contenedor
COPY . /app/

# Instalamos las dependencias necesarias para construir nuestra app
RUN npm install
RUN npm install react-scripts@3.0.1 -g
# Creamos la carpeta que usaremos para producción
RUN npm run build

# Usamos Nginx como servidor web 
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Corremos Nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]


#FROM node:current-alpine 

# WORKDIR /app

# EXPOSE 3000 

# ENV PATH /app/node_modules/.bin:$PATH

# COPY package.json package-lock.json ./

# RUN npm install --silent && npm install react-scripts@3.3.0 -g --silent

# COPY . ./

# RUN npm run build

#-------------------

#Nginx

#FROM nginx:alpine

# WORKDIR /usr/share/nginx/html

# RUN rm -rf ./*

# COPY ./build .

# ENTRYPOINT ["nginx", "-g", "daemon off;"]